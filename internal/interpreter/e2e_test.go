package interpreter

import (
	"fmt"
	"testing"

	"gitlab.com/chiswicked/clc/internal/parser"
)

func TestInterpretExpression(t *testing.T) {
	tt := []struct {
		in  string
		out string
		err error
	}{
		{
			in:  "1+2",
			out: "3",
			err: nil,
		},
		{
			in:  "1 !=2",
			out: "true",
			err: nil,
		},
		{
			in:  "1== 2",
			out: "false",
			err: nil,
		},
		{
			in:  "(1+2)*3-(2-6/2+(5-6))",
			out: "11",
			err: nil,
		},
		{
			in:  "1+3*7-(((9-0)+-56.6*-.3)+9)",
			out: "-12.98",
			err: nil,
		},
		{
			in:  "5.00!=5",
			out: "false",
			err: nil,
		},
		{
			in:  "5.00000000000001==5.0",
			out: "false",
			err: nil,
		},
		{
			in:  "5.00000000000000==5.0",
			out: "true",
			err: nil,
		},
		{
			in:  "",
			out: "",
			err: parser.ErrNoTokens,
		},
		{
			in:  "(",
			out: "",
			err: fmt.Errorf("syntax error: unexpected %v, expecting %v at [%v]", "EOF", "expression", 1),
		},
		{
			in:  "(1",
			out: "",
			err: fmt.Errorf("syntax error: unexpected %v, expecting %v at [%v]", "EOF", "binary operator or )", 2),
		},
		{
			in:  "(1@",
			out: "",
			err: fmt.Errorf("invalid character %#U at [%v]", '@', 2),
		},
		{
			in:  "(1+",
			out: "",
			err: fmt.Errorf("syntax error: unexpected %v, expecting %v at [%v]", "EOF", "expression", 3),
		},
		{
			in:  "(1-",
			out: "",
			err: fmt.Errorf("syntax error: unexpected %v, expecting %v at [%v]", "EOF", "expression", 3),
		},
		{
			in:  "(1+2",
			out: "",
			err: fmt.Errorf("syntax error: unexpected %v, expecting %v at [%v]", "EOF", "binary operator or )", 4),
		},
		{
			in:  "(1+2)*)",
			out: "",
			err: fmt.Errorf("syntax error: unexpected %v, expecting %v at [%v]", ")", "expression", 6),
		},
		{
			in:  "(1+2)/)",
			out: "",
			err: fmt.Errorf("syntax error: unexpected %v, expecting %v at [%v]", ")", "expression", 6),
		},
	}

	for _, tc := range tt {
		t.Run(tc.in, func(t *testing.T) {
			res, err := InterpretExpression(tc.in)

			if tc.out != res {
				t.Errorf("expected: %v; got %v", tc.out, res)
			}

			switch {
			case tc.err == nil && err != nil:
				t.Errorf("expected err: <nil>; got '%v'", err)
			case tc.err != nil && err == nil:
				t.Errorf("expected err: <nil>; got '%v'", err)
			case tc.err != nil && err != nil && err.Error() != tc.err.Error():
				t.Errorf("expected err: %v; got '%v'", tc.err, err)
			}

		})
	}
}
