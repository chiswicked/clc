package interpreter

import (
	"errors"
	"fmt"
	"strconv"

	"gitlab.com/chiswicked/clc/internal/core"
	"gitlab.com/chiswicked/clc/internal/eval"
	"gitlab.com/chiswicked/clc/internal/lex"
	"gitlab.com/chiswicked/clc/internal/parser"
)

const (
	floatPrecision int = 10
)

func round(f float64, n int) float64 {
	floatStr := fmt.Sprintf("%."+strconv.Itoa(n)+"f", f)
	fmt.Println(floatStr)
	inst, _ := strconv.ParseFloat(floatStr, 64)
	return inst
}

func InterpretExpression(input string) (string, error) {
	tokens, err := lex.Tokenize(input)
	if err != nil {
		return "", err
	}

	ast, err := parser.Parse(tokens)
	if err != nil {
		return "", err
	}

	e := eval.NewEvaluator()
	res := e.Eval(ast)

	switch r := res.(type) {
	case *core.CoreBool:
		return r.String(), nil
	case *core.CoreNum:
		return fmt.Sprintf("%v", round(r.Value(), floatPrecision)), nil
	case *core.CoreError:
		return "", errors.New(r.String())
	default:
		return "", errors.New("invalid evaluation result")
	}
}
