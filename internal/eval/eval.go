package eval

import (
	"fmt"

	"gitlab.com/chiswicked/clc/internal/core"
	"gitlab.com/chiswicked/clc/internal/parser"
)

type Evaluator struct {
}

func NewEvaluator() *Evaluator {
	return &Evaluator{}
}

func (e *Evaluator) Eval(node parser.Node) core.CoreObject {
	switch n := node.(type) {
	case *parser.NumericLiteralNode:
		return core.NewCoreNum(n.Value(), 0)
	// case *parser.UnaryExpressionNode:
	// 	return e.evalUnaryExpression(n)
	case *parser.BinaryExpressionNode:
		return e.evalBinaryExpression(n)
	default:
		return core.NewCoreError("unknown error", 0)
	}
}

// func (e *Evaluator) evalUnaryExpression(node *parser.UnaryExpressionNode) core.CoreObject {
// 	return nil
// }
func (e *Evaluator) evalBinaryExpression(node *parser.BinaryExpressionNode) core.CoreObject {
	left := e.Eval(node.Left())

	switch l := left.(type) {
	case *core.CoreError:
		return left
	case *core.CoreNum:
		right := e.Eval(node.Right())
		switch r := right.(type) {
		case *core.CoreError:
			return right
		case *core.CoreNum:
			if res, ok := numBinOpFuncMap[node.Op()]; !ok {
				fmt.Println(node.Op())
				return core.NewCoreError("invalid operator", 0)
			} else {
				return res(l.Value(), r.Value(), 0)
			}
		case *core.CoreBool:
			return core.NewCoreError("invalid expression", 0)
		default:
			return core.NewCoreError("invalid expression", 0)
		}
	case *core.CoreBool:
		right := e.Eval(node.Right())
		switch r := right.(type) {
		case *core.CoreError:
			return right
		case *core.CoreBool:
			fmt.Println(node.Op())
			if res, ok := boolOpFuncMap[node.Op()]; !ok {
				return core.NewCoreError("invalid operator", 0)
			} else {
				return res(l.Value(), r.Value(), 0)
			}
		case *core.CoreNum:
			return core.NewCoreError("invalid expression", 0)
		default:
			return core.NewCoreError("invalid expression", 0)
		}
	default:
		return core.NewCoreError("invalid expression", 0)
	}

}

type numBinOpFunc func(float64, float64, int) core.CoreObject

var numBinOpFuncMap = map[parser.BinaryOperator]numBinOpFunc{
	parser.ADD: numBinAddOpFunc,
	parser.SUB: numBinSubOpFunc,
	parser.MUL: numBinMulOpFunc,
	parser.DIV: numBinDivOpFunc,
	parser.EQ:  numBinEqOpFunc,
	parser.NEQ: numBinNeqOpFunc,
	parser.GT:  numBinGtOpFunc,
	parser.GTE: numBinGteOpFunc,
	parser.LT:  numBinLtOpFunc,
	parser.LTE: numBinLteOpFunc,
}

func numBinAddOpFunc(a, b float64, pos int) core.CoreObject {
	return core.NewCoreNum(a+b, pos)
}

func numBinSubOpFunc(a, b float64, pos int) core.CoreObject {
	return core.NewCoreNum(a-b, pos)
}

func numBinMulOpFunc(a, b float64, pos int) core.CoreObject {
	return core.NewCoreNum(a*b, pos)
}

func numBinDivOpFunc(a, b float64, pos int) core.CoreObject {
	if b == 0 {
		return core.NewCoreError("division by 0", pos)
	}
	return core.NewCoreNum(a/b, pos)
}

func numBinEqOpFunc(a, b float64, pos int) core.CoreObject {
	return core.NewCoreBool(a == b, pos)
}

func numBinNeqOpFunc(a, b float64, pos int) core.CoreObject {
	return core.NewCoreBool(a != b, pos)
}

func numBinGtOpFunc(a, b float64, pos int) core.CoreObject {
	return core.NewCoreBool(a > b, pos)
}

func numBinGteOpFunc(a, b float64, pos int) core.CoreObject {
	return core.NewCoreBool(a >= b, pos)
}

func numBinLtOpFunc(a, b float64, pos int) core.CoreObject {
	return core.NewCoreBool(a < b, pos)
}

func numBinLteOpFunc(a, b float64, pos int) core.CoreObject {
	return core.NewCoreBool(a <= b, pos)
}

type boolBinOpFunc func(bool, bool, int) core.CoreObject

var boolOpFuncMap = map[parser.BinaryOperator]boolBinOpFunc{
	parser.EQ:  boolBinEqOpFunc,
	parser.NEQ: boolBinNeqOpFunc,
	// parser.AND: boolBinAndOpFunc,
	// parser.OR:  boolBinOrOpFunc,
}

func boolBinEqOpFunc(a, b bool, pos int) core.CoreObject {
	return core.NewCoreBool(a == b, pos)
}

func boolBinNeqOpFunc(a, b bool, pos int) core.CoreObject {
	return core.NewCoreBool(a != b, pos)
}

// func boolBinAndOpFunc(a, b bool, pos int) core.CoreObject {
// 	return core.NewCoreBool(a && b, pos)
// }

// func boolBinOrOpFunc(a, b bool, pos int) core.CoreObject {
// 	return core.NewCoreBool(a || b, pos)
// }
