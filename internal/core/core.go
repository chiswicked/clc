package core

import (
	"fmt"
)

// type CoreType string

// const (
// 	NumType  CoreType = "NUMERIC"
// 	BoolType CoreType = "BOOLEAN"
// 	// FuncType  CoreType = "FUNCTION"
// 	ErrorType CoreType = "ERROR"
// )

// func (c CoreType) String() string {
// 	return string(c)
// }

type CoreObject interface {
	Pos() int
	String() string
}

type core struct {
	pos int
}

func (c *core) Pos() int {
	return c.pos
}

// Numeric

type CoreNum struct {
	val float64
	core
}

func NewCoreNum(val float64, pos int) *CoreNum {
	return &CoreNum{val, core{pos}}
}

func IsNumeric(o CoreObject) bool {
	_, ok := o.(*CoreNum)
	return ok
}

func (n *CoreNum) Value() float64 {
	return n.val
}

func (n *CoreNum) String() string {
	return fmt.Sprintf("%v", n.val)
}

// Boolean

type CoreBool struct {
	val bool
	core
}

func NewCoreBool(val bool, pos int) *CoreBool {
	return &CoreBool{val, core{pos}}
}

func IsBool(o CoreObject) bool {
	_, ok := o.(*CoreBool)
	return ok
}

func (b *CoreBool) Value() bool {
	return b.val
}

func (b *CoreBool) String() string {
	return fmt.Sprintf("%v", b.val)

}

// Error

type CoreError struct {
	val string
	core
}

func NewCoreError(val string, pos int) *CoreError {
	return &CoreError{val, core{pos}}
}

func IsError(o CoreObject) bool {
	_, ok := o.(*CoreError)
	return ok
}

func (e *CoreError) Value() string {
	return e.val
}

func (e *CoreError) String() string {
	return fmt.Sprintf("%v at [%v]", e.val, e.pos)
}
