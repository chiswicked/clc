package parser

import (
	"errors"
	"strconv"

	"gitlab.com/chiswicked/clc/internal/lex"
)

var (
	ErrNoTokens         = errors.New("parser: expected a list of tokens")
	ErrIndexOutOfBounds = errors.New("parser: index out of bounds")
	ErrTokenError       = errors.New("parser: expected a valid token")
	ErrNotAFactor       = errors.New("parser: expected a factor")
	ErrNotANumber       = errors.New("parser: cannot convert to number")
	ErrUnclosedParen    = errors.New("parser: unclosed parentheses")
	ErrMissingEOF       = errors.New("parser: missing EOF marker")
)

type parser struct {
	index int
	input []lex.Token
}

func Parse(tokens []lex.Token) (Node, error) {
	// Token list should have at least one token
	if len(tokens) == 0 || tokens[0].Type() == lex.TokenEOF {
		return nil, ErrNoTokens
	}

	// Token list should not have any errors
	for _, token := range tokens {
		if token.Type() == lex.TokenError {
			return nil, errors.New(token.String())
		}
	}

	// Token list should end with EOF marker
	if tokens[len(tokens)-1].Type() != lex.TokenEOF {
		return nil, ErrMissingEOF
	}

	// Parse input
	p := parser{index: 0, input: tokens}

	res, err := p.parseEquation()
	if err != nil {
		return nil, err
	}

	// Parsing should complete on EOF token,
	// otherwise parsing was terminated due to an error
	if p.currentToken().Type() != lex.TokenEOF {
		return nil, ErrTokenError
	}

	return res, nil
}

func (p *parser) currentToken() lex.Token {
	if p.index > len(p.input)-1 {
		return p.input[len(p.input)]
	}
	return p.input[p.index]
}

func (p *parser) advance() bool {
	if p.index >= len(p.input)-1 {
		return false
	}
	p.index++
	return true
}

func (p *parser) parseEquation() (Node, error) {
	res, err := p.parseExpression()
	if err != nil {
		return nil, err
	}

	for {
		t := p.currentToken()
		switch t.Type() {
		case lex.TokenEqualEqual:
			p.advance()
			right, err := p.parseExpression()
			if err != nil {
				return nil, err
			}
			res = &BinaryExpressionNode{op: EQ, left: res, right: right}
		case lex.TokenNotEqual:
			p.advance()
			right, err := p.parseExpression()
			if err != nil {
				return nil, err
			}
			res = &BinaryExpressionNode{op: NEQ, left: res, right: right}
		default:
			return res, nil
		}
	}
}

func (p *parser) parseExpression() (Node, error) {
	res, err := p.parseTerm()
	if err != nil {
		return nil, err
	}

	for {
		t := p.currentToken()
		switch t.Type() {
		case lex.TokenPlus:
			p.advance()
			right, err := p.parseTerm()
			if err != nil {
				return nil, err
			}
			res = &BinaryExpressionNode{op: ADD, left: res, right: right}
		case lex.TokenMinus:
			p.advance()
			right, err := p.parseTerm()
			if err != nil {
				return nil, err
			}
			res = &BinaryExpressionNode{op: SUB, left: res, right: right}
		default:
			return res, nil
		}
	}
}

func (p *parser) parseTerm() (Node, error) {
	res, err := p.parseFactor()
	if err != nil {
		return nil, err
	}

	for {
		t := p.currentToken()
		switch t.Type() {
		case lex.TokenMultiplication:
			p.advance()
			right, err := p.parseFactor()
			if err != nil {
				return nil, err
			}
			res = &BinaryExpressionNode{op: MUL, left: res, right: right}
		case lex.TokenDivision:
			p.advance()
			right, err := p.parseFactor()
			if err != nil {
				return nil, err
			}
			res = &BinaryExpressionNode{op: DIV, left: res, right: right}
		default:
			return res, nil
		}
	}
}

func (p *parser) parseFactor() (Node, error) {
	t := p.currentToken()
	switch t.Type() {
	case lex.TokenNumber:
		p.advance()
		num, err := strconv.ParseFloat(t.Value(), 64)
		if err != nil {
			return nil, ErrNotANumber
		}
		return &NumericLiteralNode{
			val: num,
		}, nil
	case lex.TokenLeftParen:
		p.advance()
		e, err := p.parseEquation()
		if err != nil {
			return nil, err
		}
		if p.currentToken().Type() != lex.TokenRightParen {
			return nil, ErrUnclosedParen
		}
		p.advance()
		return e, nil
	default:
		return nil, ErrNotAFactor
	}
}
