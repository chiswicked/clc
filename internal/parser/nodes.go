package parser

import (
	"fmt"

	"gitlab.com/chiswicked/clc/internal/lex"
)

type BinaryOperator string

const (
	ADD BinaryOperator = "+"
	SUB BinaryOperator = "-"
	MUL BinaryOperator = "*"
	DIV BinaryOperator = "/"
	MOD BinaryOperator = "%"
	EQ  BinaryOperator = "=="
	NEQ BinaryOperator = "!="
	LT  BinaryOperator = "<"
	LTE BinaryOperator = "<="
	GT  BinaryOperator = ">"
	GTE BinaryOperator = ">="
	AND BinaryOperator = "&&"
	OR  BinaryOperator = "||"
)

// func (op BinaryOperator) String() string {
// 	return string(op)
// }

type node struct {
	pos int
}

func (n *node) Pos() int {
	return n.pos
}

type Node interface {
	// Pos() int
	String() string
}

// type UnaryExpression interface {
// 	Op() lex.Token
// 	Right() Node
// }

type BinaryExpressionNode struct {
	op    BinaryOperator
	left  Node
	right Node
}

func (e *BinaryExpressionNode) Op() BinaryOperator {
	return e.op
}

func (e *BinaryExpressionNode) Left() Node {
	return e.left
}

func (e *BinaryExpressionNode) Right() Node {
	return e.right
}

type BinaryExpression interface {
	Op() lex.Token
	Left() Node
	Right() Node
}

// Numeric literal

type NumericLiteralNode struct {
	val float64
}

func (n *NumericLiteralNode) Value() float64 {
	return n.val
}

func (n *NumericLiteralNode) String() string {
	return fmt.Sprintf("%v", n.val)
}

// Addition operation

type AddOpNode struct {
	BinaryExpressionNode
}

// func (n *AddOpNode) Type() NodeType {
// 	return AddOpType
// }

func (n *BinaryExpressionNode) String() string {
	return fmt.Sprintf("(%s %s %s)", n.left.String(), n.op, n.right.String())
}

// Subtraction operation

type SubOpNode struct {
	BinaryExpressionNode
}

// func (n *SubOpNode) Type() NodeType {
// 	return SubOpType
// }

func (n *SubOpNode) String() string {
	return fmt.Sprintf("(%s - %s)", n.left.String(), n.right.String())
}

// Multiplication operation

type MulOpNode struct {
	BinaryExpressionNode
}

// func (n *MulOpNode) Type() NodeType {
// 	return MulOpType
// }

func (n *MulOpNode) String() string {
	return fmt.Sprintf("(%s * %s)", n.left.String(), n.right.String())
}

// Division operation

type DivOpNode struct {
	BinaryExpressionNode
}

// func (n *DivOpNode) Type() NodeType {
// 	return DivOpType
// }

func (n *DivOpNode) String() string {
	return fmt.Sprintf("(%s / %s)", n.left.String(), n.right.String())
}
