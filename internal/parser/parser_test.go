package parser

import (
	"fmt"
	"testing"

	"gitlab.com/chiswicked/clc/internal/lex"
)

func TestParse(t *testing.T) {
	tt := []struct {
		in  string
		out string
		err error
	}{
		{
			in:  ".2 == .2 != (3==3)",
			out: "((0.2 == 0.2) != (3 == 3))",
			err: nil,
		},
		{
			in:  "01+2*-8.33-+.3/.8-((0-0*7.0)+0)",
			out: "(((1 + (2 * -8.33)) - (0.3 / 0.8)) - ((0 - (0 * 7)) + 0))",
			err: nil,
		},
		{
			in:  "01+2*-8.33-+.3/.8.-((0-0*7.0)+0)",
			err: fmt.Errorf("parser: cannot convert to number"),
		},
	}

	for _, tc := range tt {
		t.Run(tc.in, func(t *testing.T) {
			tokens, _ := lex.Tokenize(tc.in)
			node, err := Parse(tokens)

			switch {
			case tc.err == nil && err != nil:
				t.Errorf("expected err: <nil>; got '%v'", err)
			case tc.err != nil && err == nil:
				t.Errorf("expected err: '%v'; got <nil>", err)
			case tc.err != nil && err != nil && err.Error() != tc.err.Error():
				t.Errorf("expected err: '%v'; got '%v'", tc.err, err)
			}

			var str string
			if node != nil {
				str = node.String()
			}

			switch {
			case err != nil && node != nil:
				t.Errorf("expected: <nil>; got '%v'", str)
			case err == nil && node == nil:
				t.Errorf("expected: %v; got <nil>", str)
			case err == nil && node != nil && tc.out != str:
				t.Errorf("expected: '%v'; got '%v'", tc.out, str)
			}
		})
	}
}
