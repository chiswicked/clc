package lex

import (
	"fmt"
	"testing"
)

func TestTokenize(t *testing.T) {
	tt := []struct {
		in  string
		out []Token
		err error
	}{
		{
			in:  "(",
			out: nil,
			err: fmt.Errorf(errUnexpectedToken, "EOF", "expression", 1),
		},
		{
			in:  "( 8",
			out: nil,
			err: fmt.Errorf(errUnexpectedToken, "EOF", "binary operator or )", 3),
		},
		{
			in: "1+2",
			out: []Token{
				{typ: TokenNumber, val: "1", pos: 0},
				{typ: TokenPlus, val: string(plusSign), pos: 1},
				{typ: TokenNumber, val: "2", pos: 2},
				{typ: TokenEOF, pos: 3},
			},
			err: nil,
		},
		{
			in: "1+2 *13.567/ (.30--67.) != 0",
			out: []Token{
				{typ: TokenNumber, val: "1", pos: 0},
				{typ: TokenPlus, val: string(plusSign), pos: 1},
				{typ: TokenNumber, val: "2", pos: 2},
				{typ: TokenMultiplication, val: string(mulSign), pos: 4},
				{typ: TokenNumber, val: "13.567", pos: 5},
				{typ: TokenDivision, val: string(divSign), pos: 11},
				{typ: TokenLeftParen, val: string(leftParen), pos: 13},
				{typ: TokenNumber, val: ".30", pos: 14},
				{typ: TokenMinus, val: string(minusSign), pos: 17},
				{typ: TokenNumber, val: "-67.", pos: 18},
				{typ: TokenRightParen, val: string(rightParen), pos: 22},
				{typ: TokenNotEqual, val: string(exclMark) + string(eqSign), pos: 24},
				{typ: TokenNumber, val: "0", pos: 27},
				{typ: TokenEOF, pos: 28},
			},
			err: nil,
		},
		{
			in: "0==555",
			out: []Token{
				{typ: TokenNumber, val: "0", pos: 0},
				{typ: TokenEqualEqual, val: string(eqSign) + string(eqSign), pos: 1},
				{typ: TokenNumber, val: "555", pos: 3},
				{typ: TokenEOF, pos: 6},
			},
			err: nil,
		},
		{
			in:  "1+2y3",
			out: nil,
			err: fmt.Errorf(errInvalidCharacter, 'y', 3),
		},
		{
			in:  " ( (  1 !- 8  ) ) ",
			out: nil,
			err: fmt.Errorf(errUnexpectedToken, string(exclMark)+string(minusSign), "binary operator", 8),
		},
		{
			in:  "1=! 8",
			out: nil,
			err: fmt.Errorf(errUnexpectedToken, string(eqSign)+string(exclMark), "binary operator", 1),
		},
		{
			in: "1+2.3.",
			out: []Token{
				{typ: TokenNumber, val: "1", pos: 0},
				{typ: TokenPlus, val: string(plusSign), pos: 1},
				{typ: TokenNumber, val: "2.3.", pos: 2},
				{typ: TokenEOF, pos: 6},
			},
			err: nil,
		},
		{
			in:  "(1+2)/)",
			out: nil,
			err: fmt.Errorf(errUnexpectedToken, ")", "expression", 6),
		},
	}

	for _, tc := range tt {
		t.Run(tc.in, func(t *testing.T) {
			tokens, err := Tokenize(tc.in)

			if len(tokens) != len(tc.out) {
				t.Errorf("expected len: %v; got %v", len(tc.out), len(tokens))
				return
			}

			switch {
			case tc.err == nil && err != nil:
				t.Errorf("expected err: <nil>; got '%v'", err)
			case tc.err != nil && err == nil:
				t.Errorf("expected err: <nil>; got '%v'", err)
			case tc.err != nil && err != nil && err.Error() != tc.err.Error():
				t.Errorf("expected err: %v; got '%v'", tc.err, err)
			}

			for i := range tc.out {
				if tokens[i].typ != tc.out[i].typ || tokens[i].val != tc.out[i].val || tokens[i].pos != tc.out[i].pos {
					t.Errorf("expected: %v; got %v", tc.out[i], tokens[i])
				}
			}
		})
	}
}
