package lex

import (
	"fmt"
	"strings"
	"unicode"
	"unicode/utf8"
)

type stateFunc func(*lexer) stateFunc

const (
	leftParen    = '('
	rightParen   = ')'
	plusSign     = '+'
	minusSign    = '-'
	mulSign      = '*'
	divSign      = '/'
	modOp        = '%'
	decimalPoint = '.'
	lineFeed     = '\n'
	exclMark     = '!'
	eqSign       = '='
	eof          = '®'

	// Keywords
	// Functions
	// pow = "pow"
	// min = "min"
	// max = "max"
)

func (l *lexer) next() (r rune) {
	if l.pos >= len(l.input) {
		l.width = 0
		return eof
	}
	r, l.width = utf8.DecodeRuneInString(l.input[l.pos:])
	l.pos += l.width
	return r
}

func (l *lexer) ignore() {
	l.start = l.pos
}

func (l *lexer) backup() {
	l.pos -= l.width
}

// func (l *lexer) peek() rune {
// 	r := l.next()
// 	l.backup()
// 	return r
// }

func (l *lexer) accept(valid string) bool {
	if strings.ContainsRune(valid, l.next()) {
		return true
	}
	l.backup()
	return false
}

func (l *lexer) acceptRun(valid string) {
	for strings.ContainsRune(valid, l.next()) {
	}
	l.backup()
}

func (l *lexer) emit(tt TokenType) {
	var tv string
	if tt != TokenError && tt != TokenEOF {
		tv = l.input[l.start:l.pos]
	}

	l.tokens = append(l.tokens, Token{
		typ: tt,
		val: tv,
		pos: l.start,
	})
	l.start = l.pos
}

func (l *lexer) errorf(format string, args ...interface{}) stateFunc {
	l.tokens = append(l.tokens, Token{
		typ: TokenError,
		val: fmt.Sprintf(format, args...),
		pos: l.start,
	})
	return nil
}

func lexExpression(l *lexer) stateFunc {
	for {
		// Expecting an operand
		switch r := l.next(); {

		// New expression within parentheses
		case r == leftParen:
			l.backup()
			for state := lexLeftParen; state != nil; {
				state = state(l)
			}

			if l.tokens[len(l.tokens)-1].Type() == TokenError {
				return nil
			}
			return lexAfterExpression

		// Constant
		case r == plusSign || r == minusSign || r == decimalPoint || isDigit(r):
			l.backup()
			lexNumber(l)
			return lexAfterExpression

		// TODO: Function

		// TODO: Unary operation

		// Whitespace
		case unicode.IsSpace(r):
			l.ignore()

		// Premature EOF
		case r == eof:
			return l.errorf(errUnexpectedToken, "EOF", "expression", l.start)

		// Other unexpected character
		default:
			return l.errorf(errUnexpectedToken, string(r), "expression", l.start)
		}
	}
}

func lexLeftParen(l *lexer) stateFunc {
	l.pos += len(string(leftParen))
	l.emit(TokenLeftParen)
	return lexInsideParens
}

func lexRightParen(l *lexer) stateFunc {
	l.pos += len(string(rightParen))
	l.emit(TokenRightParen)
	return nil
}

func lexInsideParens(l *lexer) stateFunc {
	// Lex expression inside parens
	for state := lexExpression; state != nil; {
		state = state(l)
	}

	// Expression processing should not result in error
	if l.tokens[len(l.tokens)-1].Type() == TokenError {
		return nil
	}

	// Lex next rune and branch out to relevant lexer
	for {
		switch r := l.next(); {
		case r == rightParen:
			l.backup()
			return lexRightParen
		case unicode.IsSpace(r):
			l.ignore()
		case r == eof:
			return l.errorf(errUnexpectedToken, "EOF", "binary operator or "+string(rightParen), l.start)
		default:
			l.errorf(errUnexpectedToken, string(r), "binary operator or "+string(rightParen), l.start)
		}
	}
}

func lexOperator(l *lexer) stateFunc {
	switch r := l.next(); {
	case r == plusSign:
		l.emit(TokenPlus)
	case r == minusSign:
		l.emit(TokenMinus)
	case r == mulSign:
		l.emit(TokenMultiplication)
	case r == divSign:
		l.emit(TokenDivision)
	case r == exclMark:
		if r2 := l.next(); r2 == eqSign {
			l.emit(TokenNotEqual)
		} else {
			l.backup()
			l.backup()
			return l.errorf(errUnexpectedToken, string(r)+string(r2), "binary operator", l.start)
		}
	case r == eqSign:
		if r2 := l.next(); r2 == eqSign {
			l.emit(TokenEqualEqual)
		} else {
			l.backup()
			l.backup()
			return l.errorf(errUnexpectedToken, string(r)+string(r2), "binary operator", l.start)
		}
	// case modOp:
	// 	l.emit(TokenModulo)
	default:
		return l.errorf(errUnexpectedToken, string(r), "binary operator", l.start)
	}
	return lexExpression
}

func lexNumber(l *lexer) stateFunc {
	l.accept("+-")
	l.acceptRun("0123456789.abcdefABCDEF")
	l.emit(TokenNumber)
	return nil
}

func lexAfterExpression(l *lexer) stateFunc {
	for {
		switch r := l.next(); {
		case unicode.IsSpace(r):
			l.ignore()
		case r == eof:
			return nil
		case isOperator(r):
			l.backup()
			return lexOperator
		case r == rightParen:
			l.backup()
			return nil
		default:
			return l.errorf(errInvalidCharacter, r, l.start) // ?
		}
	}
}

func isOperator(r rune) bool {
	return r == plusSign || r == minusSign || r == mulSign || r == divSign || r == modOp || r == exclMark || r == eqSign
}

func isDigit(r rune) bool {
	return '0' <= r && r <= '9'
}
