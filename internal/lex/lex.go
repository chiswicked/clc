package lex

import (
	"errors"
	"fmt"
	"strings"
)

type TokenType int

const (
	errInvalidCharacter = "invalid character %#U at [%v]"
	errUnexpectedToken  = "syntax error: unexpected %v, expecting %v at [%v]"
)

const (
	TokenError TokenType = iota
	TokenNumber
	TokenPlus
	TokenMinus
	TokenMultiplication
	TokenDivision
	TokenLeftParen
	TokenRightParen
	TokenEqualEqual
	TokenNotEqual
	TokenEOF
)

func (tt TokenType) String() string {
	return [...]string{
		"error",
		"number",
		"plus",
		"minus",
		"multiply",
		"divide",
		"left paren",
		"right paren",
		"euqal equal",
		"not equal",
		"EOF",
	}[tt]
}

type lexer struct {
	input  string
	start  int
	pos    int
	width  int
	tokens []Token
}

type Token struct {
	typ TokenType
	val string
	pos int
}

func (t Token) String() string {
	s := t.typ.String()
	if t.typ == TokenNumber || t.typ == TokenError {
		s = fmt.Sprintf("%s = %v", s, t.val)
	}
	s = fmt.Sprintf("%v at [%v]", s, t.pos)
	return s
}

func (t Token) Type() TokenType {
	return t.typ
}

func (t Token) Value() string {
	return t.val
}

func (t Token) Pos() int {
	return t.pos
}

func Tokenize(input string) ([]Token, error) {
	l := &lexer{
		input:  input,
		tokens: []Token{},
	}

	if len(strings.TrimSpace(input)) != 0 {
		for state := lexExpression; state != nil; {
			state = state(l)
		}
	}
	l.emit(TokenEOF)

	// Input processing failed
	if len(l.tokens) > 1 {
		if t := l.tokens[len(l.tokens)-2]; t.Type() == TokenError {
			return nil, errors.New(t.Value())
		}
	}
	// Input was fully processed
	return l.tokens, nil
}
