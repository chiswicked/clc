# Command Line Calculator

[![Pipeline Status](https://gitlab.com/chiswicked/clc/badges/master/pipeline.svg)](https://gitlab.com/chiswicked/clc/pipelines)
[![Coverage Report](https://gitlab.com/chiswicked/clc/badges/master/coverage.svg)](https://gitlab.com/chiswicked/clc/commits/master)

## Installation

```bash
$ go install gitlab.com/chiswicked/clc/cmd/clc@latest
```
