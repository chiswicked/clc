package main

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/chiswicked/clc/internal/interpreter"
)

func main() {
	if len(os.Args) == 1 {
		str, err := os.Executable()
		if err != nil {
			panic(err)
		}
		exe := filepath.Base(str)
		fmt.Print("clc is a command line calculator\n\n")
		fmt.Printf("Usage:\n\t%v <expression>\n\n", exe)
		fmt.Printf("Examples:\n\t%v 4+5*9\n\t%v \"3 * -5 - (8 + 9)\"\n", exe, exe)
		return
	}

	str := strings.Join(os.Args[1:], "")

	res, err := interpreter.InterpretExpression(str)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Println(res)
}
